package main

type Queue struct {
	items []string
}

func (queue *Queue) New() *Queue {
	queue.items = []string{}

	return queue
}

func (queue *Queue) Insert(item string) {
	queue.items = append(queue.items, item)
}

func (queue *Queue) RemoveFromTop() string{
	item := queue.items[0]
	queue.items = queue.items[1:len(queue.items)]

	return item
}

func (queue *Queue) RemoveItem(item string) {
	for index, value := range queue.items {
		if value  == item {
			queue.items = append(queue.items[:index], queue.items[index+1:]...)
			return
		}
	}
}