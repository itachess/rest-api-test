# Backend Test

## Requirements

GO version 1.13.4>=  
Gorilla Mux installed (Run `go get github.com/gorilla/mux`)  
Postgre SQL version 12

## Building & Running

Inside the project folder, run:
```
go build
```

Then, execute the command:

```
./rest-api-test.exe <username> <password> <database>
```

Use Postman or any other API testing tool you desire.

The server is listening in the port 8080, so use the following URL for test:

```
localhost:8080/workflow
```


## /workflow (POST)

### Body Request

The body of the /workflow request should look like:
```json
{
    "Data": "{'SuperData': 'a lot of information here'}",
    "Steps": [
        "step1",
        "step2",
        "stepInfinity"
    ]
}
```

### Possible outcomes

__Code__: 200  
__Response__: Json containing the inserted workflow

---------------------------------------------------

__Code__: 400  
__Response__: An error message that occurred while decoding the request parameters

---------------------------------------------------

__Code__: 500  
__Response__: An error message that occurred while inserting workflow in database

## /workflow (GET)

### Possible outcomes

__Code__: 200  
__Response__: A list of all workflows

---------------------------------------------------

__Code__: 200  
__Response__: A message informing that there is no workflows inserted yet.

## /workflow/{uuid} (PATCH)

### Possible outcomes

__Code__: 400  
__Response__: A message that occurred while trying to update workflow on database

---------------------------------------------------

__Code__: 200  
__Response__: A message informing that the workflow was successfuly updated

## /workflow/consume (GET)

### Possible outcomes

__Code__: 500  
__Response__: A message that occurred while trying to update workflow on database or while writing CSV file

---------------------------------------------------

__Code__: 200  
__Response__: A message informing that the workflow was successfuly consumed