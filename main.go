package main

import (
    "log"
    "net/http"
	"github.com/gorilla/mux"
	"encoding/json"
	"encoding/csv"
	"os"
	"fmt"
)

var (
	queue Queue
	workflows Workflows
)

func replyWithMessage(w http.ResponseWriter, code int, message string){
	w.Header().Set("Content-Type", "application/json")
	http.Error(w, message, code)
}

func replyWithData(w http.ResponseWriter, code int, data []Workflow){
	d, _ := json.Marshal(data)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(d)
}

func WriteCSVData(uuid string) error {
	csvFile, err := os.Create(fmt.Sprintf("%s.csv", uuid))
		
	if err != nil {
		return err
	}
	defer csvFile.Close()
	
	csvwriter := csv.NewWriter(csvFile)

	workflow := workflows.Get(uuid)

	var data []string
	data = append(data, workflow.Data)
	_ = csvwriter.Write(data)

	csvwriter.Flush()

	return nil
}

func ListWorkflows(w http.ResponseWriter, r *http.Request){
	workflowList := workflows.ReturnAll()

	if len(workflowList) == 0 {
		replyWithMessage(w, 200, "There is no workflows")
	}

	replyWithData(w, 200, workflowList)
}

func InsertWorkflow(w http.ResponseWriter, r *http.Request){

	type decoded struct {
		Data  json.RawMessage `json:"Data"`
		Steps []string        `json:"Steps"`
	}

	decoder := json.NewDecoder(r.Body)
	var d decoded

	if err := decoder.Decode(&d); err != nil {
		replyWithMessage(w, 400, "Error: " + err.Error())
		return
	}
	defer r.Body.Close()
	
	uuid, err := workflows.Set(string(d.Data), d.Steps)

	if err != nil {
		replyWithMessage(w, 500, "Error: " + err.Error())
		return
	}

	queue.Insert(uuid)

	var workflowList []Workflow
	workflow := workflows.Get(uuid)
	workflowList = append(workflowList, workflow)
	replyWithData(w, 200, workflowList)
}

func UpdateWorkflow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	uuid := vars["UUID"]

	queue.RemoveItem(uuid)
	err := workflows.Update(uuid, "Consumed")

	if err != nil {
		replyWithMessage(w, 400, "Error: " + err.Error())
	} else {
		replyWithMessage(w, 200, "Workflow updated successfuly!")
	}
}

func ConsumeWorkflow(w http.ResponseWriter, r *http.Request){
	workflowId := queue.RemoveFromTop()
	err := workflows.Update(workflowId, "Consumed")

	if err != nil {
		replyWithMessage(w, 500, "Error: " + err.Error())
	} else {
		err = WriteCSVData(workflowId)
		if err != nil {
			replyWithMessage(w, 500, "Error: " + err.Error())
			return
		}
		replyWithMessage(w, 200, "Workflow consumed successfuly!")
	}
}

func main() {

	if len(os.Args) <= 3 {
		log.Println("\nExpected arguments are: \n <user> <password> <database>\n\n The program will exit now!")
		return
	}
	userName := os.Args[1]
	password := os.Args[2]
	database := os.Args[3]

	workflows.New(userName, password, database)
	workflows.prepareDB()
	queue.New()
	
	r := mux.NewRouter()
	
    r.HandleFunc("/workflow", ListWorkflows).Methods("GET")
	r.HandleFunc("/workflow", InsertWorkflow).Methods("POST")
	r.HandleFunc("/workflow/{UUID}", UpdateWorkflow).Methods("PATCH")
	r.HandleFunc("/workflow/consume", ConsumeWorkflow).Methods("GET")

    log.Fatal(http.ListenAndServe(":8080", r))
}