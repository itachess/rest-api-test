package main

import (
	"database/sql"
	"github.com/lib/pq"
	"fmt"
)

type Workflow struct {
	UUID   string
	Status string
	Data   string
	Steps  []string
}

type Workflows struct {
	DB *sql.DB
}

func (workflows *Workflows) New(userName string, password string, database string){
	var err error
	psqlInfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", 
							userName, password, database)
	workflows.DB, err = sql.Open("postgres", psqlInfo)

	if err != nil {
		panic(err)
	}

	fmt.Println("Successfully connected!")
}

func (workflows *Workflows) prepareDB(){

	createStatusTypeQuery := `DO $$ BEGIN
    						     CREATE TYPE status_t AS ENUM ('Inserted', 'Consumed');
							  EXCEPTION
    						     WHEN duplicate_object THEN null;
							  END $$;`

	if _, err := workflows.DB.Exec(createStatusTypeQuery); err != nil {
		panic(err)
	}

	fmt.Println("Type successfully created!")

	createExtensionQuery := `CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`

	if _, err := workflows.DB.Exec(createExtensionQuery); err != nil {
		panic(err)
	}


	createTableQuery := `CREATE TABLE IF NOT EXISTS workflows (
		uuid UUID DEFAULT uuid_generate_v1mc(),
		status status_t DEFAULT 'Inserted',
		data JSONB,
		steps text[],
		PRIMARY KEY (uuid)
	);`

	if _, err := workflows.DB.Exec(createTableQuery); err != nil {
		panic(err)
	}

	fmt.Println("Table Successfully created!")

	return
}

func (workflows *Workflows) Get(uuid string) Workflow {
	var workflow Workflow
	workflow.UUID = uuid

	workflows.DB.QueryRow(
		`SELECT status, data, steps FROM workflows WHERE uuid=$1`,
		uuid).Scan(&workflow.Status, &workflow.Data, pq.Array(&workflow.Steps))

	return workflow
}

func (workflows *Workflows) Set(Data string, Steps []string) (string, error) {
	var uuid string
	err := workflows.DB.QueryRow("INSERT INTO workflows(data, steps) VALUES($1, $2) RETURNING uuid", 
								Data, pq.Array(&Steps)).Scan(&uuid)

	return uuid, err
}

func (workflows *Workflows) Update(uuid string, status string) error {
	_, err := workflows.DB.Exec(
		"UPDATE workflows SET status=$1 WHERE uuid=$2",
		status, uuid)

	return err
}

func (workflows *Workflows) ReturnAll() []Workflow {
	rows, err := workflows.DB.Query("SELECT uuid, status, data, steps FROM workflows")
	defer rows.Close()

	if err != nil {
		panic(err)
	}

	var workflowList []Workflow

	for rows.Next() {
		var workflow Workflow
		err := rows.Scan(&workflow.UUID, &workflow.Status, 
							&workflow.Data, pq.Array(&workflow.Steps))

		if err != nil {
			panic(err)
		}
		workflowList = append(workflowList, workflow)
	}

	return workflowList
}